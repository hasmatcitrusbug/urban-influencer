
  
var mySidenav = document.getElementById("mySidenav");
var nav = document.getElementById("nav-res");
var backdrop = document.getElementById("backdrop");
var wrapper = document.getElementById("wrapper");
var body = document.getElementById("body");

function openNav() {

  mySidenav.classList.add("width80");
  nav.classList.add("opacityon");
  backdrop.classList.add("display-block");
  wrapper.classList.add("position-fixed");
  body.classList.add("overflow-fixed");
  
}

document.getElementById("backdrop").addEventListener("click", backdrop_click);

function backdrop_click() {
  mySidenav.classList.remove("width80");
  nav.classList.remove("opacityon");
  backdrop.classList.remove("display-block");
  wrapper.classList.remove("position-fixed");
  body.classList.remove("overflow-fixed");
}

function closeNav() {
  mySidenav.classList.remove("width80");
  nav.classList.remove("opacityon");
  backdrop.classList.remove("display-block");
  wrapper.classList.remove("position-fixed");
  body.classList.remove("overflow-fixed");
} 

/* end of navigation */

/* Set navigation */
AOS.init({
  once: true
});